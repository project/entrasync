<?php

namespace Drupal\entrasync\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\entrasync\Services\EntraSync;
use Drupal\entrasync\Entity\SyncEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AdminPageController.
 *
 * Controller for administrative pages of the EntraSync module.
 */
class SyncController extends ControllerBase {

  /**
   * The EntraSync service.
   *
   * @var Drupal\entrasync\Services\EntraSync
   */
  protected $entraSync;

  /**
   * Constructs an AdminPageController object.
   *
   * @param Drupal\entrasync\Services\EntraSync $entraSync
   *   The EntraSync service.
   *
   */
  public function __construct(EntraSync $entraSync) {
    $this->entraSync = $entraSync;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entrasync.entra_sync'),
    );
  }

  public function performSync(SyncEntity $entrasync) {
    $this->entraSync->fullSync($entrasync);
    return $this->redirect('entrasync.collection');
  }

  public function updateSyncStatus() {
    $this->messenger()->addMessage('Nothing to see here yet.');
    return $this->redirect('entrasync.collection');
  }

}
