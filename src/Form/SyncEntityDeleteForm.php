<?php

namespace Drupal\entrasync\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines a confirmation form for deleting a sync entity.
 */
class SyncEntityDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    // You can customize this question to be more specific or user-friendly.
    return $this->t('Are you sure you want to delete the entity %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    // This should return the URL to the listing page of your entities, or wherever you'd like the user to go back to upon cancellation.
    return new Url('entrasync.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    // This description provides additional information to the user.
    return $this->t('This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Delete the entity and show a confirmation message.
    $entity = $this->getEntity();
    $entity->delete();

    $this->messenger()->addMessage($this->t('Entity %label has been deleted.', ['%label' => $entity->label()]));

    // Redirect to the listing page after deletion.
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    // Unique ID for the delete form.
    return 'entrasync_sync_entity_delete_form';
  }

}
