<?php

namespace Drupal\entrasync\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SyncEntityForm extends EntityForm {

  /**
   * The entity type manager service.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager service.
   *
   * @var Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new SyncSettingsForm.
   *
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager,
                              EntityFieldManagerInterface $entityFieldManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
    );
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // Add some CSS to the filter section.
    $form['#attached']['library'][] = 'entrasync/query-filter';

    $entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $entity->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => ['Drupal\entrasync\Entity\SyncEntity', 'load'],
        'source' => ['label'],
      ],
      '#disabled' => !$entity->isNew(),
      '#required' => TRUE,
    ];

    // Start of connection settings details.
    $form['graph_api_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Graph API settings'),
      '#required' => TRUE,
      '#open' => TRUE,
    ];

    // Set the graph key
    $form['graph_api_settings']['graph_key'] = [
      '#type' => 'key_select',
      '#key_filters' => ['type' => 'ms_graph_api'],
      '#title' => $this->t('MS Graph Authentication Key'),
      '#default_value' => $entity->get('graph_key'),
      '#required' => TRUE,
      '#key_description' => FALSE,
      '#description' => $this->t('Choose an available key. If the desired key is not listed, <a href=":link">create a new key</a> of type "MS Graph API Key".',
                              [':link' => Url::fromRoute('entity.key.add_form')->toString()]),
    ];

    // Set the properties to fetch from the graph.
    $form['graph_api_settings']['graph_properties'] = [
      '#type' => 'checkboxes',
      '#options' => $this->getAllEntraProperties(),
      '#title' => $this->t('Properties to fetch'),
      '#description' => $this->t('Select the properties to fetch from Graph API. Refer to <a href=":doc">properties documentation</a> to see what they return. Note that some might need additional permissions.',
                                [':doc' => 'https://learn.microsoft.com/en-us/graph/api/resources/user?view=graph-rest-1.0#properties)']),
      '#default_value' => $entity->get('graph_properties') ?: array_keys($this->getDefaultEntraProperties()),
      '#ajax' => [
        'callback' => '::propertyChangeCallbacks',
      ],
    ];

    // Make 'mail' checkbox disabled, as it should always be fetched.
    $form['graph_api_settings']['graph_properties']['mail']['#disabled'] = TRUE;

    // Start of general settings details.
    $form['general_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('General setting'),
    ];

    // Filter incoming users or not.
    $form['general_settings']['filter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Filter the incoming users.'),
      '#default_value' => $entity->get('filter'),
      '#description' => $this->t('Filter the incoming users to limit the number of users to sync.'),
    ];

    // Container for filter options.
    $form['general_settings']['container'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['entrasync-filter-query-container']],
      '#states' => [
        'visible' => [
          ':input[name="filter"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="filter"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Select which property to filter.
    $form['general_settings']['container']['entrafilter_property'] = [
      '#type' => 'select',
      '#title' => $this->t('Entra property'),
      '#options' => $this->getSelectedEntraProps(),
      '#default_value' => $entity->get('entrafilter_property'),
      '#prefix' => '<div id="entrafilter-property-wrapper">',
      '#suffix' => '</div>',
      '#states' => [
        'visible' => [
          ':input[name="filter"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="filter"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Select what to filter the property by.
    $form['general_settings']['container']['entrafilter_operator'] = [
      '#type' => 'select',
      '#title' => 'that..',
      '#default_value' => $entity->get('entrafilter_operator') ?: 'startswith',
      '#options' => [
        'startswith'  => $this->t('Starts with'),
        'endswith'    => $this->t('Ends with'),
        'contains'    => $this->t('Contains'),
        'notcontains' => $this->t('Does not contain'),
        'equals'      => $this->t('Equals'),
        'notequals'   => $this->t('Does not equal'),
        'empty'       => $this->t('Is empty'),
        'notempty'    => $this->t('Is not empty'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="filter"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="filter"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // If the filter needs a value, set it here. Disable for filters that doesn't need a value.
    $form['general_settings']['container']['entrafilter_value'] = [
      '#type' => 'textfield',
      '#title' => 'Text',
      '#default_value' => $entity->get('entrafilter_value'),
      '#states' => [
        'visible' => [
          ':input[name="filter"]' => ['checked' => TRUE],
          // placing each value in an array is an implicit "or".
          [':input[name="entrafilter_operator"]' => ['!value' => 'notempty']],
          [':input[name="entrafilter_operator"]' => ['!value' => 'empty']],
        ],
        'required' => [
          ':input[name="filter"]' => ['checked' => TRUE],
          [':input[name="entrafilter_operator"]' => ['!value' => 'notempty']],
          [':input[name="entrafilter_operator"]' => ['!value' => 'empty']],
        ],
      ],
    ];

    // Help text for filters.
    $filter_help_text = '<p>Select what to filter users by after they have been collected. You use this to limit which users to process.</p>';
    $form['general_settings']['entrafilter_helptext'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['entrasync-filter-query-helptext']],
      '#markup' => '<small>' . $this->t($filter_help_text) . '</small>',
      '#states' => [
        'visible' => [
          ':input[name="filter"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Fetch new users on cron, or not.
    $form['general_settings']['retrieve_on_cron'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Syncronize new users on cron'),
      '#default_value' => $entity->get('retrieve_on_cron') ?? TRUE,
      '#description' => $this->t('Enable to check for new users each time cron runs. If disabled you will need
                                  to do a manual syncronization.')
    ];

    // Store and use a delta query to only get new or updated users, or always sync all users.
    $form['general_settings']['delta_query'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only synchronize new or updated users'),
      '#default_value' => $entity->get('delta_query') ?? TRUE,
      '#description' => $this->t('By default only your initial sync will get all users, while
                                        subsequent requests only will get new or updated users (using a delta query).
                                        If you always want to get every users in the tenant, uncheck this.')
    ];

    // Add a select element for entity type to map to, per now only user is supported.
    $entity_options = ['user' => 'User', 'node' => 'Node'];

    $form['general_settings']['entrauser_entities'] = [
      '#type' => 'radios',
      '#title' => $this->t('Map Entra users to Drupal entity'),
      '#options' => $entity_options,
      '#default_value' => 'user',
      '#multiple' => FALSE,
      '#description' => $this->t('Select which Drupal entities the new users should be added to (per now only user is supported)'),
      '#attributes' => ['disabled' => ['node']],
    ];

    // Start of entra user settings details.
    $form['entrauser_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Drupal user configuration'),
    ];

    $form['entrauser_settings']['username_mapping'] = [
      '#type' => 'select',
      '#title' => $this->t('Username mapping'),
      '#options' => $this->getSelectedEntraProps(),
      '#default_value' => $entity->get('username_mapping') ?: 'mail',
      '#prefix' => '<div id="username-mapping-wrapper">',
      '#suffix' => '</div>',
      '#description' => $this->t('The username must be unique, so in most cases you would use e-mail or UPN.'),
    ];

    // Field mappings stored in config.
    $stored_mappings = $entity->get('user_field_mapping') ?: [];

    foreach($this->getDrupalUserFields() as $key => $value) {
      $default_value = $stored_mappings[$key] ?? NULL;

      $form['entrauser_settings']['drupal_field_' . $key] = [
        '#type' => 'select',
        '#title' => $this->t('Drupal field:') . " " . $value,
        '#prefix' => '<div id="drupal_field_' . $key . '">',
        '#suffix' => '</div>',
        '#options' => $this->getSelectedEntraProps(),
        '#default_value' => $default_value,
        '#empty_option' => $this->t('- Entra property -'),
      ];
    }

    // Add a select element for roles to modify on import.
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    unset($roles['authenticated']);
    unset($roles['anonymous']);

    $role_options = [];
    foreach ($roles as $role_id => $role) {
      $role_options[$role_id] = $role->label();
    };

    $form['entrauser_settings']['modify_entrauser_roles'] = [
      '#type' => 'select',
      '#title' => $this->t('Modify roles on Entra users'),
      '#options' => $role_options,
      '#default_value' => $entity->get('modify_entrauser_roles'),
      '#multiple' => TRUE,
      '#description' => $this->t('Select which roles the new users should or should not have'),
    ];

    // Add a select element to chose initial state of the imported user.
    $user_status_options = ['blocked' => 'Blocked', 'active' => 'Active'];

    $form['entrauser_settings']['entrauser_status'] = [
      '#type' => 'radios',
      '#title' => $this->t('Initial state of imported user'),
      '#options' => $user_status_options,
      '#default_value' => $entity->get('entrauser_status') ?: 'blocked',
      '#multiple' => FALSE,
      '#description' => $this->t('Select wether the user should be created as blocked or active account.'),
    ];

    $form['entrauser_settings']['send_mail_on_activate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send activation e-mail when creating user'),
      '#default_value' => $entity->get('send_mail_on_activate', FALSE),
      '#states' => [
        'visible' => [
          ':input[name="entrauser_status"]' => ['value' => 'active'],
        ],
      ],
      '#description' => $this->t('Enable to send the Welcome (new user created by administrator) e-mail when account is created.
                                  Edit the e-mail in <a href=":link">Account settings</a>',
                                [':link' => Url::fromRoute('entity.user.admin_form')->toString()]),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /**
     * @todo Based on old logic, validation works but message and vars should be updated.
     */

    $user_field_mapping = $this->getUserFieldMappingsFromFormState($form_state);
    $selected_values = [];
    foreach ($user_field_mapping as $entra_field => $drupal_field) {
      if (!empty($drupal_field)) {
        if (isset($selected_values[$drupal_field])) {
          // Set an error if the same Drupal field is selected more than once.
          $form_state->setErrorByName('user_field_to_' . $entra_field, $this->t('The Drupal field %field is already mapped to another Entra field.', ['%field' => $drupal_field]));
        }
        else {
          $selected_values[$drupal_field] = $entra_field;
        }
      }
    }
  }


  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // dsm($form_state->getValues(), 'form state from save / submitForm');
    // Graph key handling.
    $entity->set('graph_key', $form_state->getValue('graph_key'));

    // Graph properties handling. Filter out empty values, and store the keys.
    $entity->set('graph_properties', array_keys(array_filter($form_state->getValue('graph_properties'))));

    // Graph filters handling.
    $entity->set('filter', $form_state->getValue('filter'));
    $entity->set('entrafilter_property', $form_state->getValue('entrafilter_property'));
    $entity->set('entrafilter_operator', $form_state->getValue('entrafilter_operator'));
    $entity->set('entrafilter_value', $form_state->getValue('entrafilter_value'));

    // Cron settings handling.
    $entity->set('retrieve_on_cron', $form_state->getValue('retrieve_on_cron'));

    // Delta query handling
    $entity->set('delta_query', $form_state->getValue('delta_query'));

    // Username mapping
    $entity->set('username_mapping', $form_state->getValue('username_mapping'));

    // Map to which Drupal entity settings handling.
    $mapped_drupal_entity = $form_state->getValue('entrauser_entities');
    $mapped_drupal_entity = (array) ($mapped_drupal_entity);
    $entity->set('mapped_drupal_entities', $mapped_drupal_entity);

    // Get array of drupalfields => entraProps
    $mapped_drupalfields = $this->getUserFieldMappingsFromFormState($form_state);

    // Remove any mappings that have an empty value.
    $mapped_drupalfields = array_filter($mapped_drupalfields);
    $entity->set('user_field_mapping', $mapped_drupalfields);

    // Role settings handling.
    $modify_entrauser_roles = $form_state->getValue('modify_entrauser_roles');
    $modify_entrauser_roles = empty($modify_entrauser_roles) ? [] : $modify_entrauser_roles;
    $entity->set('modify_entrauser_roles', array_keys($modify_entrauser_roles));

    // Initial user state handling (blocked or active)
    $entrauser_status = $form_state->getValue('entrauser_status');
    $entity->set('entrauser_status', $entrauser_status);

    // E-mail settings if user is set to active.
    $email_on_active = $form_state->getValue('send_mail_on_activate');
    $entity->set('send_mail_on_activate', $email_on_active);

    /**
     * @todo This has no corresponding settings field
     *
     */
    // $modify_drupaluser_roles = $form_state->getValue('modify_drupaluser_roles');
    // $modify_drupaluser_roles = empty($modify_drupaluser_roles) ? [] : $modify_drupaluser_roles;

    $status = $entity->save();

    if ($status) {
      // Entity was saved.
      $this->messenger()->addMessage($this->t('The entity %label has been saved.', ['%label' => $entity->label()]));
    } else {
      // Entity save failed.
      $this->messenger()->addError($this->t('The entity %label was not saved.', ['%label' => $entity->label()]));
    }
    $form_state->setRedirect('entrasync.collection');
  }

  /**
   * Get all Entra properties.
   *
   * @return array
   *   Array of all Entra properties, with human readable name as value.
   */
  public function getAllEntraProperties() : array {
    return [
      'mail'              => 'Mail',
      'userPrincipalName' => 'User Principal Name',
      'givenName'         => 'Given Name',
      'displayName'       => 'Display Name',
      'surname'           => 'Surname',
      'businessPhones'    => 'Business Phones',
      'mobilePhone'       => 'Mobile Phone',
      'officeLocation'    => 'Office Location',
      'department'        => 'Department',
      'jobTitle'          => 'Job Title',
      'companyName'       => 'Company Name',
      'postalCode'        => 'Postal Code',
      'state'             => 'State',
      'streetAddress'     => 'Street Address',
      'city'              => 'City',
      'country'           => 'Country',
      'accountEnabled'    => 'Account Enabled',
      'ageGroup'          => 'Age Group',
      'creationType'      => 'Creation Type',
      'employeeHireDate'  => 'Employee Hire Date',
      'employeeId'        => 'Employee ID',
      'employeeOrgData'   => 'Employee Org Data',
      'employeeType'      => 'Employee Type',
      'externalUserState' => 'External User State',
      'faxNumber'         => 'Fax Number',
      'imAddresses'       => 'IM Addresses',
      'mailNickname'      => 'Mail Nickname',
      'otherMails'        => 'Other Mails',
      'preferredLanguage' => 'Preferred Language',
      'usageLocation'     => 'Usage Location',
      'userType'          => 'User Type',
    ];
  }

  /**
   * Get default Entra properties.
   *
   * @return array
   *   Array of default Entra properties, with human readable name as value.
   */
  private function getDefaultEntraProperties() {
    return [
      'mail'              => 'Mail',
      'userPrincipalName' => 'User Principal Name',
      'displayName'       => 'Display Name',
      'givenName'         => 'Given Name',
      'surname'           => 'Surname',
      'jobTitle'          => 'Job Title',
      'businessPhones'    => 'Business Phones',
      'mobilePhone'       => 'Mobile Phone',
      'officeLocation'    => 'Office Location',
      'department'        => 'Department',
    ];
  }

  /**
   * Get all Entra properties.
   *
   * @return array
   *   Array of Entra properties selected for retrieval, based on stored props
   *   or default props.
   */
  public function getSelectedEntraProps() {
    // Either stored properties or default
    $selected_properties = $this->entity->get('graph_properties') ?: array_keys($this->getDefaultEntraProperties());
    $all_properties = $this->getAllEntraProperties();

    // Uses the readable value from getAllEntraProperties() in the option select.
    $property_options = [];
    foreach ($selected_properties as $property) {
      if (isset($all_properties[$property])) {
        $property_options[$property] = $this->t($all_properties[$property]);
      }
    }
    return $property_options;
  }

  /**
   * Get available Drupal user fields.
   *
   * @return array
   *   Array of available user fields.
   */
  private function getDrupalUserFields() {
    // Fetch Drupal user fields, including custom fields.
    $user_fields = $this->entityFieldManager->getFieldDefinitions('user', 'user');

    // Exclude fields we don't want to overwrite, and include only custom fields.
    $exclude_user_fields = [
      'name',
      'mail',
      'uuid',
      'uid',
      'langcode',
      'created',
      'changed',
      'access',
      'login',
      'status',
      'timezone',
      'roles',
      'langcode',
      'preferred_langcode',
      'preferred_admin_langcode',
      'init',
      'pass',
      'timezone',
      'default_langcode',
      'path',
    ];

    // Filter the options so we get only fields we want.
    $custom_fields = [];

    // @todo, figure best way to do this. Entra will typically only provide string values.
    foreach ($user_fields as $field_name => $field_definition) {
      if (!in_array($field_name, $exclude_user_fields)
          && $field_definition->getType() != 'entity_reference'
          && $field_definition->getType() != 'image'
          && $field_definition->getType() != 'file') {
        $custom_fields[$field_name] = $field_definition->getLabel();
      }
    }

    return $custom_fields;
  }


  /**
   * The AJAX callback when updating the properties to fetch.
   *
   * @todo I want to also rebuild the field mappings form, but
   * it is only partly rebuild, without proper classname, and thus
   * cannot be processed further.
   *
   */
  public function propertyChangeCallbacks(array &$form, FormStateInterface $form_state) {
    $updated_elements = $this->updatePropertyOptions($form, $form_state);;

    // Add each updated form to the corresponding item.
    $response = new AjaxResponse();
    foreach ($updated_elements as $selector => $updated_form) {
      $response->addCommand(new ReplaceCommand('#' . $selector, $updated_form));
    }

    return $response;
  }

  /**
   * Update property options.
   *
   * Ajax callback to update the properties that is currently selected.
   *
   * @param array $form
   * @param FormStateInterface $form_state
   *
   * @return array
   *   An array of updated form elements, e.g. all elements that makes use of the options.
   */
  public function updatePropertyOptions(array &$form, FormStateInterface $form_state) : array {
    $selected_properties = array_filter($form_state->getValue('graph_properties'));

    // Build new options based on the selected properties.
    $options = [];
    $options[NULL] = $this->t('- Entra property - ');
    $all_properties = $this->getAllEntraProperties();
    foreach ($selected_properties as $property) {
      if (isset($all_properties[$property])) {
        $options[$property] = $this->t($all_properties[$property]);
      }
    }

    // Update the filter options of the entrafilter_property select elements
    $form['general_settings']['container']['entrafilter_property']['#options'] = $options;

    // Update the options for the username mapping.
    $form['entrauser_settings']['username_mapping']['#options'] = $options;

    // Update the options for any Drupal field.
    $updated_elements = [];
    foreach ($this->getDrupalUserFields() as $key => $label) {
      $form['entrauser_settings']['drupal_field_' . $key]['#options'] = $options;
      $updated_elements['drupal_field_' . $key] = $form['entrauser_settings']['drupal_field_' . $key];
    }

    // Add other updated elements.
    $updated_elements['entrafilter-property-wrapper'] = $form['general_settings']['container']['entrafilter_property'];
    $updated_elements['username-mapping-wrapper'] = $form['entrauser_settings']['username_mapping'];

    return $updated_elements;
  }

  /**
   * Extracts user field mappings from form state.
   *
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   An associative array of user field mappings, in format 'drupal_field' => 'entraProperty'.
   */
  protected function getUserFieldMappingsFromFormState(FormStateInterface $form_state) : array {
    $user_field_mapping = [];
    foreach ($form_state->getValues() as $key => $value) {
      if (strpos($key, 'drupal_field_') === 0) {
        $drupal_field = substr($key, strlen('drupal_field_'));
        $user_field_mapping[$drupal_field] = $value;
      }
    }
    return $user_field_mapping;
  }

}
