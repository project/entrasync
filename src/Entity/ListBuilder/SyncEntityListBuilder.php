<?php

namespace Drupal\entrasync\Entity\ListBuilder;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a class to build a listing of sync entities.
 */
class SyncEntityListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Name');
    $header['info'] = $this->t('Information');
    $header['operations'] = $this->t('Operations');
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {

    $row['id'] = [
      'data' => [
        '#type' => 'markup',
        '#markup' => '<strong>' . $entity->label() . '</strong><br />' .
                      '<small>id: <em>' . $entity->id() . '</em></small>',
        '#allowed_tags' => ['strong', 'br', 'small'],
      ],
    ];

    // Get the graph information for the help text.
    $used_key = \Drupal::service('key.repository')->getKey($entity->get('graph_key'));
    $key_info = $this->t('<strong>Graph: </strong>using @key_label to fetch @num_properties properties<br />',
                        [
                          '@key_label' => $used_key->label(),
                          '@num_properties' => count($entity->get('graph_properties'))
                        ]);

    // Get the filter information for the help text.
    $has_filter = $entity->get('filter');
    $filter_property = $entity->get('entrafilter_property');
    $filter_operator = $entity->get('entrafilter_operator');
    $operator_mapping = [
      'startswith'  => $this->t('Starts with'),
      'endswith'    => $this->t('Ends with'),
      'contains'    => $this->t('Contains'),
      'notcontains' => $this->t('Does not contain'),
      'equals'      => $this->t('Equals'),
      'notequals'   => $this->t('Does not equal'),
      'empty'       => $this->t('Is empty'),
      'notempty'    => $this->t('Is not empty'),
    ];
    $human_readable_operator = $operator_mapping[$filter_operator] ?? $filter_operator;
    $filter_value = $entity->get('entrafilter_value');

    $filter_info = '';
    if (!empty($has_filter)) {
      $filter_info = $this->t('<strong>Filter: </strong>@filter_property that @filter_operator @filter_value<br />', [
        '@filter_property' => $filter_property,
        '@filter_operator' => lcfirst($human_readable_operator),
        '@filter_value' => $filter_value,
      ]);
    }

    // Get the mapping information for the help text.
    $mapped_to_entity = $entity->get('entrauser_entities');
    $num_fields_mapped = count($entity->get('user_field_mapping'));
    $mapping_info = $this->t('<strong>Mapping: </strong>@num_fields fields mapped to @mapped_to_entity entity <br />', [
      '@num_fields' => $num_fields_mapped,
      '@mapped_to_entity' => $mapped_to_entity,
    ]);

    // Get the sync settings for the help text.
    $cron_settings = $entity->get('retrieve_on_cron');
    $delta_query = $entity->get('delta_query');

    // Determine the message parts based on the settings.
    $cron_message = $cron_settings ? $this->t('Data is fetched during cron.') : $this->t('Data must be fetched manually (not using cron).');
    $delta_message = $delta_query ? $this->t('Only new or updated users are fetched.') : $this->t('All users are fetched always.');

    // Combine the messages into the full help text.
    $sync_settings_info = $this->t('<strong>Settings: </strong>@cron_message @delta_message <br />', [
      '@cron_message' => $cron_message,
      '@delta_message' => $delta_message,
    ]);

    // Build the actual help text:
    $row['info'] = [
      'data' => [
        '#type' => 'markup',
        '#markup' => $key_info . $filter_info . $mapping_info . $sync_settings_info,
        '#allowed_tags' => ['strong', 'br', 'small'],
      ],
    ];

    // Get the default operations.
    $operations = $this->buildOperations($entity)['#links'];

    // Add custom operations.
    $operations['perform_sync'] = [
      'title' => $this->t('Perform Sync'),
      'url' => $entity->toUrl('sync'),
      'weight' => 10,
    ];
    $operations['update_sync_status'] = [
      'title' => $this->t('Update Status'),
      'url' => $entity->toUrl('update-status'),
      'weight' => 15,
    ];

    $row['operations']['data'] = [
      '#type' => 'operations',
      '#links' => $operations,
    ];

    return $row;
  }

}
