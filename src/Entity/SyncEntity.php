<?php

namespace Drupal\entrasync\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Entrasync entity.
 *
 * @ConfigEntityType(
 *   id = "entrasync",
 *   label = @Translation("Entrasync config"),
 *   handlers = {
 *     "list_builder" = "Drupal\entrasync\Entity\ListBuilder\SyncEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\entrasync\Form\SyncEntityForm",
 *       "edit" = "Drupal\entrasync\Form\SyncEntityForm",
 *       "delete" = "Drupal\entrasync\Form\SyncEntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "entrasync.entity",
 *   admin_permission = "administer entra sync settings",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "graph_key",
 *     "graph_properties",
 *     "filter",
 *     "entrafilter_property",
 *     "entrafilter_operator",
 *     "entrafilter_value",
 *     "retrieve_on_cron",
 *     "delta_query",
 *     "entrauser_entities",
 *     "send_mail_on_activate",
 *     "entrauser_status",
 *     "send_mail_on_activate",
 *     "modify_entrauser_roles",
 *     "username_mapping",
 *     "user_field_mapping",
 *   },
 *   links = {
 *      "canonical" = "/admin/structure/entrasync/{entrasync}",
 *      "add-form" = "/admin/structure/entrasync/add",
 *      "edit-form" = "/admin/structure/entrasync/{entrasync}/edit",
 *      "delete-form" = "/admin/structure/entrasync/{entrasync}/delete",
 *      "collection" = "/admin/structure/entrasync",
 *      "sync" = "/admin/structure/entrasync/{entrasync}/sync",
 *      "update-status" = "/admin/structure/entrasync/{entrasync}/update-status",
 *   }
 * )
 */
class SyncEntity extends ConfigEntityBase {

  /**
   * The Entrasync ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Entrasync label.
   *
   * @var string
   */
  protected $label;

  /**
   * The MS Graph API key.
   *
   * @var string
   */
  protected $graph_key;

  /**
   * The MS Graph API properties.
   *
   * @var array
   */
  protected $graph_properties = [];

  /**
   * The Filter status
   *
   * @var string
   */
  protected $filter;

  /**
   * The Entra user property to filter.
   *
   * @var string
   */
  protected $entrafilter_property;

  /**
   * The filter function to use on the incoming users
   *
   * @var string
   */
  protected $entrafilter_operator;

  /**
   * The filter variable to use on the incoming users
   *
   * @var string
   */
  protected $entrafilter_value;

  /**
   * The cron setting.
   *
   * @var bool
   */
  protected $retrieve_on_cron;

  /**
   * The delta query setting.
   *
   * @var bool
   */
  protected $delta_query;

  /**
   * The entity to map to setting.
   *
   * @var string
   */
  protected $entrauser_entities;

  /**
   * The user entity status on import setting.
   *
   * @var string
   */
  protected $entrauser_status;

  /**
   * The send mail on user activation setting.
   *
   * @var string
   */
  protected $send_mail_on_activate;

  /**
   * The roles that should be set on imported users.
   *
   * @var array
   */
  protected $modify_entrauser_roles = [];

  /**
   * Username mapping from Entra to Drupal users. Can either be UPN or email.
   *
   * @var string
   */
  protected $username_mapping;

  /**
   * Field mappings from Entra to Drupal users.
   *
   * @var array
   */
  protected $user_field_mapping = [];


}
