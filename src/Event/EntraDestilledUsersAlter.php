<?php

namespace Drupal\entrasync\Event;

use Drupal\Component\EventDispatcher\Event;

class EntraDestilledUsersAlter extends Event {
  const NAME = 'entrasync.destilled_users_alter';

  /**
   * The array of destilled user info
   *
   * @var array
   */
  protected $destilledUsers;

  /**
   * The id of the sync entity
   *
   * @var string
   */
  protected $syncEntityId;

  /**
   * @inheritDoc
   *
   */
  public function __construct(array $destilledUsers, string $syncEntityId) {
    $this->destilledUsers = $destilledUsers;
    $this->syncEntityId = $syncEntityId;
  }

  /**
   * Get sync entity id
   *
   * The id of the sync entity.
   *
   * @return string
   */
  public function getId() : string {
    return $this->syncEntityId;
  }

  /**
   * Gets destilled users.
   *
   * Gets an array of all users from Entra, where the properties are extrached
   *
   * @return array
   */
  public function getUsers() : array {
    return $this->destilledUsers;
  }

  /**
   * Sets user array as modified by event.
   *
   * @param array $alteredUsers
   *    Array, possibly filtered by custom logic.
   *
   * @return void
   */
  public function setUsers(array $alteredUsers) : void {
    $this->destilledUsers = $alteredUsers;
  }

}
