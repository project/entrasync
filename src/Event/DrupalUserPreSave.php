<?php

namespace Drupal\entrasync\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\user\Entity\User;

class DrupalUserPreSave extends Event {
  const NAME = 'entrasync.drupal_user_pre_save';

  protected $user;
  protected $data;
  
  /** 
   * The id of the sync entity
   *
   * @var string
   */
  protected $syncEntityId;

  public function __construct(User $user, array $data, string $syncEntityId) {
    $this->user = $user;
    $this->data = $data;
    $this->syncEntityId = $syncEntityId;
  }

  public function getId() : string {
    return $this->syncEntityId;
  }

  public function getUser() {
    return $this->user;
  }

  public function getData() {
    return $this->data;
  }
}
