<?php

namespace Drupal\entrasync\Plugin\QueueWorker;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Password\PasswordGeneratorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\entrasync\Entity\SyncEntity;
use Drupal\entrasync\Event\DrupalUserPreSave;
use Drupal\user\Entity\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Entra users.
 *
 * @QueueWorker(
 *   id = "entrasync_user_processor",
 *   title = @Translation("Entrasync User Processor"),
 *   cron = {"time" = 60}
 * )
 */
class EntraUserProcessor extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The config.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The password generator.
   *
   * @var \Drupal\Core\Password\PasswordGeneratorInterface
   */
  protected $passwordGenerator;

  /**
   * The event dispatcher.
   *
   * @var EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory.
   * @param MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Password\PasswordGeneratorInterface $passwordGenerator
   *   The password generator.
   * @param EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   *
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              LoggerChannelFactoryInterface $loggerFactory,
                              MessengerInterface $messenger,
                              PasswordGeneratorInterface $passwordGenerator,
                              EventDispatcherInterface $eventDispatcher) {
    // $this->configFactory = $configFactory;
    // $this->config = $configFactory->get('entrasync.settings'); // this is wrong, needs to use the plugin id
    $this->logger = $loggerFactory->get('entrasync');
    $this->messenger = $messenger;
    $this->passwordGenerator = $passwordGenerator;
    $this->eventDispatcher = $eventDispatcher;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('password_generator'),
      $container->get('event_dispatcher'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data)
  {
    if (!isset($data['user'], $data['sync_entity_id'])) {
      $this->logger->error('Queue item is missing required data.');
      return;
    }

    // Loading config and user data.
    $syncEntityId = $data['sync_entity_id'];

    /** @var \Drupal\entrasync\Entity\SyncEntity $syncEntity */
    $syncEntity = SyncEntity::load($syncEntityId);
    if (!$syncEntity) {
      $this->logger->error('Sync entity could not be loaded for ID: ' . $syncEntityId);
      return;
    }

    // Check if there is an email field on the Entra user
    if (empty($data['user']['mail'])) {
      $msg = 'Empty email field for user: ' . $data['user']['userprincipalname'] . ' in ' . $syncEntityId . '. User not added to Drupal';
      $this->logger->error($msg);
      $this->messenger->addError($msg);
      return;
    }

    // Check if the user exist in Drupal
    $user_exists = user_load_by_mail($data['user']['mail']);

    try {
      if ($user_exists === false) {
        // Set username to chosen value, or e-mail.
        $selectedUserName = isset($data['user'][$syncEntity->get('username_mapping')]) ? $data['user'][$syncEntity->get('username_mapping')] : $data['user']['mail'];

        $user = User::create();
        $user->setPassword($this->passwordGenerator->generate());
        $user->enforceIsNew();
        $user->setEmail($data['user']['mail']);
        $user->setUsername($selectedUserName);
        }
      else {
        // User exists, load and update it.
        $user = $user_exists;
      }

      // Handle field mappings.
      $this->applyUserData($user, $data, $syncEntity);

      // Handle roles.
      $roles_to_modify = $syncEntity->get('modify_entrauser_roles');
      foreach ($roles_to_modify as $role_id) {
        if (!$user->hasRole($role_id)) {
          $user->addRole($role_id);
        }
        // @todo Additional logic for removing roles should be added here
      }

      // Handle activation status based on the syncEntity configuration.
      if ($syncEntity->get('entrauser_status') === 'active') {
        $user->activate();
      } else {
        $user->block();
      }

      // Dispatch event to allow other modules to alter the user creation process
      $event = new DrupalUserPreSave($user, $data, $syncEntity->get('id'));
      $this->eventDispatcher->dispatch($event, DrupalUserPreSave::NAME);

      // Save changes.
      $user->save();

      // Send email notification if applicable.
      if ($user->isNew() && $syncEntity->get('send_mail_on_activate')) {
        _user_mail_notify('register_admin_created', $user);
      }

    } catch (\Exception $e) {
      $this->logger->error('User processing failed: ' . $e->getMessage());
    }
  }

  /**
   * Map values from selected properties to available Drupal fields
   *
   * @param [type] $user
   * @param [type] $data
   * @param [type] $syncEntity
   * @return void
   */
  protected function applyUserData(&$user, $data, $syncEntity) : void {
    // Set custom fields based on the mapping.
    $user_field_mapping = $syncEntity->get('user_field_mapping');
    foreach ($user_field_mapping as $drupal_field => $entra_prop) {
      if (isset($data['user'][$entra_prop]) && $data['user'][$entra_prop] !== '') {
        // some properties (like business phone) return arrays which we can't handle properly, so flattening them.
        $field_value = is_array($data['user'][$entra_prop]) ? implode(', ', $data['user'][$entra_prop]) : $data['user'][$entra_prop];
        if ($user->hasField($drupal_field)) {
          $user->set($drupal_field, $field_value);
        } else {
          $this->logger->error('The field ' . $drupal_field . ' does not exist on the user entity.');
        }
      }
    }
  }

}
