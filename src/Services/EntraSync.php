<?php

namespace Drupal\entrasync\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Url;
use Drupal\entrasync\Entity\SyncEntity;
use Drupal\entrasync\Event\EntraDestilledUsersAlter;
use Drupal\ms_graph_api\GraphApiGraphFactory;
use Microsoft\Graph\Model\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


/**
 *
 */
class EntraSync {
  use StringTranslationTrait;

  /**
   * Queue names
   *
   */
  const QUEUE_ENTRAUSERS = 'entrasync_user_processor';
  // const QUEUE_ENTRADRUPALUSERS = 'entra_drupal_user_processor';

  /**
   * The entity type manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Microsoft Graph API client.
   *
   * @var Drupal\ms_graph_api\GraphApiGraphFactory
   */
  protected $graphFactory;

  /**
   * The Microsoft Graph API client.
   *
   * @var \Microsoft\Graph\Graph
   */
  protected $graphClient;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The event dispatcher
   *
   * @var EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The module handler.
   *
   * @var ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The state service.
   *
   * @var Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new EntraSync object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Microsoft\Graph\GraphFactory $MicrosoftGraphApiFactory
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   * @param ModuleHandlerInterface $moduleHandler
   * @param Drupal\Core\State\StateInterface $state
   *
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager,
                       GraphApiGraphFactory $MicrosoftGraphApiFactory,
                       QueueFactory $queueFactory,
                       MessengerInterface $messenger,
                       LoggerChannelFactoryInterface $loggerFactory,
                       EventDispatcherInterface $eventDispatcher,
                       ModuleHandlerInterface $moduleHandler,
                       StateInterface $state
                       ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->graphFactory = $MicrosoftGraphApiFactory;
    $this->queueFactory = $queueFactory;
    $this->messenger = $messenger;
    $this->logger = $loggerFactory->get('entrasync');
    $this->eventDispatcher = $eventDispatcher;
    $this->moduleHandler = $moduleHandler;
    $this->state = $state;
  }

  /**
   * Retrieves the status of user synchronization.
   *
   * @todo Everything
   *
   * @return array An associative array of synchronization status information.
   */
  public function getStatus() {

    // For an administrator overseeing the synchronization between Drupal users and Microsoft Entra (Azure AD), providing insightful statuses can greatly assist in monitoring and managing the system effectively. Here are some useful statuses you might consider:
    // Last Synchronization Time: Date and time of the last successful synchronization. This helps in understanding the currency of the data.
    // Number of Users in Drupal: The current count of users in the Drupal system.
    // Number of Users in Entra (Azure AD): The current count of users in Entra.
    // Number of Users Synced: How many users are successfully synchronized between the two systems.
    // Number of Users Pending Sync: Users existing in one system but not yet synced to the other.
    // Recent Sync Errors/Issues: Any errors or issues encountered during the last sync process, with timestamps.
    // Status of Last Sync: Whether the last sync was successful, partially successful (with some errors), or failed.
    // User Sync Mismatch Details: Specific details about any discrepancies between the user data in Drupal and Entra, such as mismatched user roles or profiles.
    // Duration of the Last Sync Process: How long the last synchronization process took.
    // Manual Intervention Required: Flag or notice if any manual intervention is required for specific cases or errors.
    // Audit Logs Link: A direct link to detailed logs or an audit trail for more in-depth analysis.
    // System Health Status: Overall health status of the sync system, possibly with a simple color-coded indicator (green for healthy, yellow for warnings, red for critical issues).
    // Scheduled Next Sync: If synchronization is done on a schedule, show the next planned sync time.
    // User Actions Required: Any actions that the administrator needs to take, such as approving user access, resolving conflicts, etc.
    // API Call Statistics: Information about API usage, rate limits, and any related errors, if applicable.
    // Version Information: Display the version of the sync software or module being used, useful for troubleshooting and updates.
    // Implementing the Statuses
    // These statuses can be implemented in the getStatus method of your synchronization class or a similar utility. Depending on your system's complexity, you might also need additional methods or services to gather this information. Remember, presenting this data in a clear, concise, and user-friendly manner in the Drupal admin interface is key to its usefulness.
    // $entraUserCount = '33';
    // $drupalUserCount = '53';
    // Return [
    //   'entraUserCount' => $entraUserCount,
    //   'drupalUserCount' => $drupalUserCount,
    // ];.
  }

  /**
   * Retrieves a list of users from Microsoft Entra.
   *
   * @param EntraSyncEntity $syncEntity
   *   The sync entity.
   *
   * @return array
   *   An array of destilled user information from Entra, based on the fetched props and filters.
   */
  public function getEntraUsersList($syncEntity) : array {
    // Building API client from key.
    $graphKey = $syncEntity->get('graph_key');
    $client = $this->graphFactory->buildGraphFromKeyId($graphKey);

    // Starting logging.
    $getCollectionTimeStart = microtime(TRUE);

    // Creating the deltalink for this entity. The deltalink only fetches the updated
    // users from the tenant, since last deltalink was used.
    $stateKey = 'entrasync_deltalink_' . $syncEntity->get('id');

    // Get the properties to fetch.
    $propertiesToFetch = $syncEntity->get('graph_properties');
    // Always add id, even if hidden in the UI.
    $propertiesToFetch[] = 'id';

    // Format the properties correctly for the query string.
    $separetedProperties = implode(',', $propertiesToFetch);

    // Create the query string.
    $queryString = "/users/delta?\$count=true&\$select={$separetedProperties}";

    // Get the deltalink, and use that instead, if it exists.
    $deltaLink = $this->state->get($stateKey);

    // Use the deltalink if not disabled, and it is not the first sync.
    if ($syncEntity->get('delta_query') !== 0 && !empty($deltaLink)) {
      $queryString = $deltaLink;
    };

    // Create the request and fetch the users.
    try {
      $userCollectionRequest = $client->createCollectionRequest("GET", $queryString)
                                     ->setReturnType(User::class)
                                     ->addHeaders(["ConsistencyLevel" => "eventual"]);

      // Fetching all users in all pages.
      $allUsers = [];
      while (!$userCollectionRequest->isEnd()) {
        $usersPage = $userCollectionRequest->getPage();
        foreach ($usersPage as $user) {
          $allUsers[] = $user;
        }
      }

      // Store the next deltalink.
      $deltalink = $userCollectionRequest->getDeltaLink();
      $this->state->set($stateKey, $deltalink);

      // Getting only the information we need from the user classes.
      $destilledEntraUserInfo = [];

      foreach ($allUsers as $user) {
        // Constructing the corresponding getter methods for each chosen property, e.g. "getPropertyName()"
        foreach ($propertiesToFetch as $property) {
          if (method_exists($user, 'get' . ucfirst($property))) {
            $userDetails[$property] = $user->{'get' . ucfirst($property)}();
          }
        }
        $destilledEntraUserInfo[] = $userDetails;
      }

      // Dispatch event to allow other modules to alter the destilled users array.
      $userAlterEvent = new EntraDestilledUsersAlter($destilledEntraUserInfo, $syncEntity->get('id'));
      $this->eventDispatcher->dispatch($userAlterEvent, EntraDestilledUsersAlter::NAME);

      // Retrieve the modified user arrayfrom potential subscribers.
      $destilledEntraUserInfo = $userAlterEvent->getUsers();

      // Adding some logging:
      $numberOfUsersFetched = count($destilledEntraUserInfo);
      $getCollectionTimeStop = microtime(TRUE);
      $getCollectionExecutionTime = sprintf("%.2f", ($getCollectionTimeStop - $getCollectionTimeStart));

      // Extra info if Queue UI module is enabled.
      if ($this->moduleHandler->moduleExists('queue_ui')) {
        $queueOverview = Url::fromRoute('queue_ui.overview_form')->toString();
        $queueProcessor = Url::fromRoute('queue_ui.process', ['queueName' => $this::QUEUE_ENTRAUSERS])->toString();
      };

      // Define the base log message with placeholders.
      $messageTemplate = 'Fetching @numberOfUsersFetched Entra users from @syncEntityLabel took @getCollectionExecutionTime seconds.';

      // Append the link placeholder if the queue_ui module is enabled.
      if (isset($queueOverview) && ($syncEntity->get('filter') == null)) {
        $messageTemplate .= ' Go to <a href=":queueOverview">queue overview</a> or <a href=":queueProcessor">process queue immediately</a>.';
      }

      $logMessage = $this->t($messageTemplate, [
        '@numberOfUsersFetched' => $numberOfUsersFetched,
        '@syncEntityLabel' => $syncEntity->label(),
        '@getCollectionExecutionTime' => $getCollectionExecutionTime,
        ':queueOverview' => $queueOverview ?? '',
        ':queueProcessor' => $queueProcessor ?? '',
      ]);

      $this->logger->notice($logMessage);
      $this->messenger->addMessage($logMessage);

      return $destilledEntraUserInfo;
    }
    catch (\Throwable $e) {
      $this->logger->error($e->getMessage());
      $this->messenger->addMessage($e->getMessage());
      return [];
    }
  }

  /**
   * Filter / reduce users from Entra based on specific criteria.
   *
   * @param object $syncEntity
   *  An object containing filter criteria for reducing Entra users.
   * @param array $allUsersFromEntra
   *  An array of all Entra users, each user represented as an array.
   *
   * @return array
   *  A filtered array of Entra users.
   */
  function reduceUsersFromEntra($syncEntity, $allUsersFromEntra) {
    $filterProperty = $syncEntity->get('entrafilter_property');
    $filterOperator = $syncEntity->get('entrafilter_operator');
    $filterValue = $syncEntity->get('entrafilter_value');

    $remainingUsers = array_filter($allUsersFromEntra, function ($user) use ($filterProperty, $filterValue, $filterOperator) {
      // Check for the property in the user object. A few properties returns arrays, so we flatten them.
      $userValue = is_array($user[$filterProperty]) ? implode(', ', $user[$filterProperty]) : $user[$filterProperty];

      // Skip if the user doesn't have a value in the property to filter for.
      if($userValue === null) {
        return;
      }

      switch ($filterOperator) {
        case 'contains':
          return str_contains($userValue, $filterValue);
        case 'notcontains':
          return !str_contains($userValue, $filterValue);
        case 'startswith':
          return str_starts_with($userValue, $filterValue);
        case 'endswith':
          return str_ends_with($userValue, $filterValue);
        case 'equals':
          return $userValue === $filterValue;
        case 'notequals':
          return $userValue !== $filterValue;
        case 'empty':
          return empty($userValue);
        case 'notempty':
          return !empty($userValue);
        default:
          return false;
      }
    });

    // Adding logging.
    $count  = count($remainingUsers);
    $humanOperator = [
      'contains' => 'contains',
      'notcontains' => 'does not contain',
      'startswith' => 'starts with',
      'endswith' => 'ends with',
      'equals' => 'equals',
      'notequals' => 'does not equal',
      'notempty' => 'is not empty',
      'empty'    => 'is empty',
    ];

     // Extra info if Queue UI module is enabled.
     if ($this->moduleHandler->moduleExists('queue_ui')) {
      $queueOverview = Url::fromRoute('queue_ui.overview_form')->toString();
      $queueProcessor = Url::fromRoute('queue_ui.process', ['queueName' => $this::QUEUE_ENTRAUSERS])->toString();
    };

    // Define the base log message.
    $messageTemplate = '@count users added to queue, where @property @operator @value.';

    // Append if queue_ui module is enabled and something is added to the queue.
    if (isset($queueOverview) && ($count > 0)) {
      $messageTemplate .= ' Go to <a href=":queueOverview">queue overview</a> or <a href=":queueProcessor">process queue immediately</a>.';
    }

    $logMessage = $this->t($messageTemplate, [
      '@count' => $count,
      '@property' => $filterProperty,
      '@operator' => $humanOperator[$filterOperator],
      '@value' => $filterValue,
      ':queueOverview' => $queueOverview ?? '',
      ':queueProcessor' => $queueProcessor ?? '',
    ]);

    $this->messenger->addMessage($logMessage);
    $this->logger->notice($logMessage);

    return $remainingUsers;
  }

  /**
   * Retrieves a list of Drupal users.
   *
   * @deprecated version 2.2.0
   *
   * @return array An array of Drupal user information.
   */
  public function getDrupalUsersList() : Array {
    // Getting all drupal user objects.
    $userStorage = $this->entityTypeManager->getStorage('user');
    $query = $userStorage->getQuery();
    $uids = $query
      ->accessCheck(TRUE)
      ->condition('uid', 1, '!=')
      ->execute();

    /** @var \Drupal\user\UserInterface $users */
    $users = $userStorage->loadMultiple($uids);

    // Destilling the objects to only get an array of emails.
    $drupalUserList = [];
    foreach ($users as $user) {
      $userDetails = [
        'email' => $user->getEmail(),
      ];
      $drupalUserList[] = $userDetails;
    }
    return $drupalUserList;
  }

  /**
   * Compares user lists from Entra and Drupal to identify unique and common users.
   *
   * @deprecated version 2.2.0
   *   We're not using this anymore.
   *
   * @param array $entraUsers
   *   An array of users from Entra.
   * @param array $drupalUsers
   *   An array of Drupal users.
   *
   * @return array An associative array categorizing users.
   *
   * @todo Add a count per array and return and/or log these values as well.
   */
  public function compareUserLists(Array $entraUsers, Array $drupalUsers) : Array {
    $drupalEmails = array_column($drupalUsers, 'email');

    $onlyInEntra = [];
    $onlyInDrupal = [];
    $inBoth = [];

    foreach ($entraUsers as $entraUser) {
      if (in_array($entraUser['mail'], $drupalEmails)) {
        // User is in both Entra and Drupal.
        $inBoth[] = $entraUser;
      }
      else {
        // User is only in Entra.
        $onlyInEntra[] = $entraUser;
      }
    }

    foreach ($drupalUsers as $drupalUser) {
      if (!in_array($drupalUser['email'], array_column($entraUsers, 'mail'))) {
        // User is only in Drupal.
        $onlyInDrupal[] = $drupalUser;
      }
    }

    // Return the categorized user objects.
    return [
      'onlyInEntra' => $onlyInEntra,
      'onlyInDrupal' => $onlyInDrupal,
      'inBoth' => $inBoth,
    ];
  }

  /**
   * Delegating users present only in Entra to a separate queue.
   *
   * @param array $entraUsers
   *   An array of Entra users.
   */
  public function processEntraUsers(Array $entraUsers, SyncEntity $syncEntity) {
    $queue = $this->queueFactory->get($this::QUEUE_ENTRAUSERS);

    if (!empty($entraUsers)) {
      $userCount = count($entraUsers);
      $this->messenger->addMessage("Adding $userCount Entra users to queue from " . $syncEntity->label());

      foreach ($entraUsers as $user) {
        $item = [
          'user' => $user,
          'sync_entity_id' => $syncEntity->id(),
        ];
        $queue->createItem($item);
      }
    }
  }

  /**
   * Delegating users present only in Drupal to a separate queue.
   *
   * @deprecated version 2.2.0
   *   Not using this anymore.
   *
   * @param array $drupalUsers
   *   An array of Drupal users.
   */
//  public function processUsersOnlyInDrupal(Array $drupalUsers) {
//
//    $queue = $this->queueFactory->get($this::QUEUE_ENTRADRUPALUSERS);
//    // Add each user to the queue.
//    foreach ($drupalUsers as $user) {
//      $this->logger->notice('Adding Drupal user to queue: ' . $user['email']);
//      $queue->createItem($user);
//    }
//  }

  /**
   * Prepares data for synchronization.
   *
   * @deprecated version 2.2.0
   *
   * @return array An array containing data for synchronization.
   */
  public function prepareSyncData($syncEntity) : Array {
    return $this->compareUserLists($this->getEntraUsersList($syncEntity), $this->getDrupalUsersList());
  }

  /**
   * Synchronizes users between Entra and Drupal.
   *
   * @param SyncEntity $syncEntity
   *   The sync entity.
   */
  public function fullSync($syncEntity) {
    // get all users from Entra tenant.
    $allUsersFromEntra = $this->getEntraUsersList($syncEntity);
    $desiredUsersFromEntra = $allUsersFromEntra;

    // if result from Entra should be reduced / filtered before processing.
    if ($syncEntity->get('filter')) {
      $desiredUsersFromEntra = $this->reduceUsersFromEntra($syncEntity, $allUsersFromEntra);
    }

    // compare the users from Entra with the ones in Drupal.
    // $allUsers = $this->compareUserLists($desiredUsersFromEntra, $this->getDrupalUsersList());

    // Some additional logic in case we want to separate these in the future.
    // onlyInEntra = new users in Entra
    // inBoth = users from Entra, already in Drupal
    $this->processEntraUsers($desiredUsersFromEntra, $syncEntity);

    // $this->processEntraUsers($allUsers['inBoth'], $syncEntity);

    // @todo
    // figure out what do to with these. They are either deleted users, or users that belong in Drupal regardless of Entra.
    // $this->processUsersOnlyInDrupal($allUsers['onlyInDrupal']);

  }
}
