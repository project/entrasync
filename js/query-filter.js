document.addEventListener('DOMContentLoaded', function() {
  const propertySelect = document.getElementById('edit-entrafilter-property');
  const operatorSelect = document.getElementById('edit-entrafilter-operator');

  if (propertySelect && operatorSelect) {
    // Function to update operator options
    const updateOperators = function() {

      const selectedProperty = propertySelect.value;

      const operators = drupalSettings.entrasync.entraPropertyOperators[selectedProperty] || [];
      operatorSelect.innerHTML = ''; // Clear current options

      // Populate new options
      operators.forEach(function(operator) {
        const option = document.createElement('option');
          option.value = operator;
            switch(operator) {
              case 'eq': option.textContent = 'Equals'; break;
              case 'ne': option.textContent = 'Does not equal'; break;
              case 'startsWith': option.textContent = 'Starts with'; break;
              case 'endsWith': option.textContent = 'Ends with'; break;
            }
          operatorSelect.appendChild(option);
      });
    };

    // Listen for changes on the property select
    propertySelect.addEventListener('change', updateOperators);
  }
});
